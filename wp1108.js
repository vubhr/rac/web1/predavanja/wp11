const TGUMB = document.getElementById("gumb");
const TDIV  = document.getElementById("tekst"); 

function mijenjaj(objekt){
   if (objekt.innerHTML == "Toggle"){
	   objekt.innerHTML = "Promijeni";
   }else{
	   objekt.innerHTML = "Toggle";
   }
   
   TDIV.classList.toggle("stil");
   TDIV.classList.toggle("hide");
}

TGUMB.addEventListener("click", function(){mijenjaj(this);}, false);
TGUMB.addEventListener("click", function(){console.log("Kliknuo sam gumb");}, false);
