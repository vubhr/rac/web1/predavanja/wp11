const AREA = document.body; //konstanta preko koje ćemo pratiti evente body-u
const CIRCLE = document.querySelector('.circle'); //dohvati element s klasom .circle

var windowWidth = window.innerWidth; //dohvaćamo širinu window-a
var windowHeight = window.innerHeight; //dohvaćamo visinu window-a

function mouseCoordinates(e) {
    // Prati event objekt (mouse movement).
    // Dohvaća koordinatu X (udaljenost od lijevog ruba body-a) preko clientX svojstva.
    // Uzima cijelu širinu body-a, oduzme trenutne koordinate i promjer kruga.
    // Slično vrijedi i za Y koordinatu (udaljenost od vrha body-a).
    var horizontalPosition = windowWidth - e.clientX - 26;
    var verticalPosition= windowHeight - e.clientY - 26;

    // Postavi horizontalnu i vertikalnu poziciju
    CIRCLE.style.left = horizontalPosition + 'px';
    CIRCLE.style.top = verticalPosition + 'px';
}

function changeColorOnTouch() {
    CIRCLE.style.backgroundColor = "green";
    CIRCLE.style.borderColor = "green";
}

// Kada se miš pomakne pokreni funkciju mouseCoordinates.
AREA.addEventListener('mousemove', mouseCoordinates, false);

// Kada miš dotakne krug (event listener je postavljen na objekt CIRCLE) pokreni funkciju changeColorOnTouch.
CIRCLE.addEventListener('mouseenter', changeColorOnTouch, false);

// Kada miš napusti krug, promijeni stil objekta (event listener je postavljen na objekt CIRCLE).
CIRCLE.addEventListener('mouseleave', function(){CIRCLE.removeAttribute("style");}, false);