const MIN = 0;
const MAX = 36;
var testNumber = 15;
var i = 1;

while (MAX) {
    let randomValue = Math.floor(Math.random() * (MAX - MIN)) + MIN;

    if (randomValue == testNumber) {
        break;
    }

    console.log("Korak " + i + ": " + randomValue);
    i++;
}

console.log("Na " + i + ". iteraciji skripta je našla testni broj " + testNumber + ".");
