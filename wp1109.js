for (var i= 0; i< 10; i++){
   console.log("for petlja " + i);
}


var n = 0;
while(n < 10){
   console.log("while petlja " + n);
   n++;
} 


var k = 0;
do{ 
   ++k;
   console.log("DO-WHILE " + k);

} while(k < 10);


var liHandle = document.getElementsByTagName("li");
console.log(liHandle);

for(var i = 0; i < liHandle.length; i++) {
   console.log(liHandle[i]);
}

//ili ovako pa daje isti rezultat
Object.values(liHandle).forEach(function(element) {
  console.log(element);
});

var a = ["5", "10", "15", "20", "25"];
a.forEach(function(element){
	console.log(element);
})


